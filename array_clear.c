#include <wchar.h>

#include "vn_terminal_input.h"

/* clear one element in array */
void
clear_one_element(wchar_t *words, unsigned int i)
{
	unsigned int length = wcslen(words);

	for ( ; i < length; ++i) {
		words[i+1] = words[i+2];
	}
}

/* clear two element in array */
void
clear_two_element(wchar_t *words, unsigned int i)
{
	clear_one_element(words, i);
	clear_one_element(words, i);
}

void
clear_words(wchar_t *words)
{
	for(unsigned int i = 0U; i < MAX; ++i)
		words[i] = '\0';
}
