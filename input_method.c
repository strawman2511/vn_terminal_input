#include <wchar.h>

#include "vn_terminal_input.h"

void
telex_method(wchar_t *words, unsigned int i)
{
	switch (words[i]){
	case 'A':
		switch (words[i+1]){
		/* Â */
		case 'A': case 'a':
			words[i] = L'\x00C2';
				
			switch (words[i+2]){
			/* Ầ */
			case 'F': case 'f':
				words[i] = L'\x1EA6';
				clear_two_element(words, i);
				break;

			/* Ậ */	
			case 'J': case 'j':
				words[i] = L'\x01EAC';
				clear_two_element(words, i);
				break;

			/* Ẩ */
			case 'R': case 'r':
				words[i] = L'\x1EA8';
				clear_two_element(words, i);
				break;

			/* Ấ */
			case 'S': case 's':
				words[i] = L'\x1EA4';
				clear_two_element(words, i);
				break;

			/* Ẫ */
			case 'X': case 'x':
				words[i] = L'\x1EAA';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;
				
		/* À */
		case 'F': case 'f':
			words[i] = L'\x00C0';
			clear_one_element(words, i);
			break;
		
		/* Ạ */	
		case 'J': case 'j':
			words[i] = L'\x1EA0';
			clear_one_element(words, i);
			break;

		/* Ả */
		case 'R': case 'r':
			words[i] = L'\x1EA2';
			clear_one_element(words, i);
			break;

		/* Á */
		case 'S': case 's':
			words[i] = L'\x00C1';
			clear_one_element(words, i);
			break;
					
		/* Ă */
		case 'W': case 'w':
			words[i] = L'\x0102';
				
			switch (words[i+2]){
			/* Ằ */
			case 'F': case 'f':
				words[i] = L'\x1EB0';
				clear_two_element(words, i);
				break;

			/* Ặ */
			case 'J': case 'j':
				words[i] = L'\x1EB6';
				clear_two_element(words, i);
				break;

			/* Ẳ */
			case 'R': case 'r':
				words[i] = L'\x1EB2';
				clear_two_element(words, i);
				break;

			/* Ắ */
			case 'S': case 's':
				words[i] = L'\x1EAE';
				clear_two_element(words, i);
				break;

			/* Ẵ */
			case 'X': case 'x':
				words[i] = L'\x1EB4';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;

		/* Ã */
		case 'X': case 'x':
			words[i] = L'\x00C3';
			clear_one_element(words, i);
	    		break;	       
		}
		break;

	case 'a':
		switch (words[i+1]){
		/* â */
		case 'A': case 'a':
			words[i] = L'\x00E2';
				
			switch (words[i+2]){
			/* ầ */
			case 'F': case 'f':
				words[i] = L'\x1EA7';
				clear_two_element(words, i);
				break;
						
			/* ậ */
			case 'J': case 'j':
				words[i] = L'\x1EAD';
				clear_two_element(words, i);
				break;

			/* ẩ */
			case 'R': case 'r':
				words[i] = L'\x1EA9';
				clear_two_element(words, i);
				break;

			/* ấ */
			case 'S': case 's':
				words[i] = L'\x1EA5';
				clear_two_element(words, i);
				break;

			/* ẫ */
			case 'X': case 'x':
				words[i] = L'\x1EAB';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;

		/* à */
		case 'F': case 'f':
			words[i] = L'\x00E0';
			clear_one_element(words, i);
			break;
			
		/* ạ */
		case 'J': case 'j':
			words[i] = L'\x1EA1';
			clear_one_element(words, i);
			break;

		/* ả */
		case 'R': case 'r':
			words[i] = L'\x1EA3';
			clear_one_element(words, i);
			break;

		/* á */
		case 'S': case 's':
			words[i] = L'\x00E1';
			clear_one_element(words, i);
			break;
			
		/* ă */	
		case 'W': case 'w':
			words[i] = L'\x0103';
					
			switch (words[i+2]){
			/* ằ */
			case 'F': case 'f':
				words[i] = L'\x1EB1';
				clear_two_element(words, i);
				break;

			/* ặ */
			case 'J': case 'j':
				words[i] = L'\x1EB7';
				clear_two_element(words, i);
				break;

			/* ẳ */
			case 'R': case 'r':
				words[i] = L'\x1EB3';
				clear_two_element(words, i);
				break;

			/* ắ */
			case 'S': case 's':
				words[i] = L'\x1EAF';
				clear_two_element(words, i);
				break;

			/* ẵ */
			case 'X': case 'x':
				words[i] = L'\x1EB5';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;

		/* ã */
		case 'X': case 'x':
			words[i] = L'\x00E3';
			clear_one_element(words, i);
	       		break;	       
		}
		break;

	/* Đ */
	case 'D':
		if (words[i+1] == 'D' || words[i+1] == 'd'){
			words[i] = L'\x0110';
			clear_one_element(words, i);
		}
		break;

	/* đ */
	case 'd':
		if (words[i+1] == 'D' || words[i+1] == 'd'){
			words[i] = L'\x0111';
			clear_one_element(words, i);
		}	
		break;

	case 'E':
		switch (words[i+1]){
		/* Ê */
		case 'E': case 'e':
			words[i] = L'\x00CA';

			switch (words[i+2]){
			/* Ề */ 
			case 'F': case 'f':
				words[i] = L'\x1EC0';
				clear_two_element(words, i);
				break;
					
			/* Ệ */	
			case 'J': case 'j':
				words[i] = L'\x1EC6';
				clear_two_element(words, i);
				break;
						
			/* Ể */
			case 'R': case 'r':
				words[i] = L'\x1EC2';
				clear_two_element(words, i);
				break;

			/* Ế */
			case 'S': case 's':
				words[i] = L'\x1EBE';
				clear_two_element(words, i);
				break;

			/* Ễ */
			case 'X': case 'x':
				words[i] = L'\x1EC4';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;	
			}
			break;

		/* È */
		case 'F': case 'f':
			words[i] = L'\x00C8';
			clear_one_element(words, i);
			break;

		/* Ẹ */
		case 'J': case 'j':
			words[i] = L'\x1EB8';
			clear_one_element(words, i);
			break;

		/* Ẻ */
		case 'R': case 'r':
			words[i] = L'\x1EBA';
			clear_one_element(words, i);
			break;

		/* É */
		case 'S': case 's':
			words[i] = L'\x00C9';
			clear_one_element(words, i);
			break;

		/* Ẽ */
		case 'X': case 'x':
			words[i] = L'\x1EBC';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'e':
		switch (words[i+1]){
		/* ê */
		case 'E': case 'e':
			words[i] = L'\x00EA';

			switch (words[i+2]){
			/* ề */
			case 'F': case 'f':
				words[i] = L'\x1EC1';
				clear_two_element(words, i);
				break;

			/* ệ */
			case 'J': case 'j':
				words[i] = L'\x1EC7';
				clear_two_element(words, i);
				break;

			/* ể */
			case 'R': case 'r':
				words[i] = L'\x1EC3';
				clear_two_element(words, i);
				break;
						
			/* ế */
			case 'S': case 's':
				words[i] = L'\x1EBF';
				clear_two_element(words, i);
				break;

			/* ễ */
			case 'X': case 'x':
				words[i] = L'\x1EC5';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;
				
		/* è */
		case 'F': case 'f':
			words[i] = L'\x00E8';
			clear_one_element(words, i);
			break;

		/* ẹ */
		case 'J': case 'j':
			words[i] = L'\x1EB9';
			clear_one_element(words, i);
			break;

		/* ẻ */
		case 'R': case 'r':
			words[i] = L'\x1EBB';
			clear_one_element(words, i);
			break;

		/* é */
		case 'S': case 's':
			words[i] = L'\x00E9';
			clear_one_element(words, i);
			break;

		/* ẽ */
		case 'X': case 'x':
			words[i] = L'\x1EBD';	
			clear_one_element(words, i);
			break;
		}
		break;

	case 'I':
		switch (words[i+1]){
		/* Ì */
		case 'F': case 'f':
			words[i] = L'\x00CC';
			clear_one_element(words, i);
			break;

		/* Ị */
		case 'J': case 'j':
			words[i] = L'\x1ECA';
			clear_one_element(words, i);
			break; 
				
		/* Ỉ */
		case 'R': case 'r':
			words[i] = L'\x1EC8';
			clear_one_element(words, i);
			break;

		/* Í */
		case 'S': case 's':
			words[i] = L'\x00CD';
			clear_one_element(words, i);
			break;

		/* Ĩ */
		case 'X': case 'x':
			words[i] = L'\x0128';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'i':
		switch (words[i+1]){
		/* ì */
		case 'F': case 'f':
			words[i] = L'\x00EC';
			clear_one_element(words, i);
			break;	

		/* ị */
		case 'J': case 'j':
			words[i] = L'\x1ECB';
			clear_one_element(words, i);
			break;

		/* ỉ */
		case 'R': case 'r':
			words[i] = L'\x1EC9';
			clear_one_element(words, i);
			break;

		/* í */
		case 'S': case 's':
			words[i] = L'\x00ED';
			clear_one_element(words, i);
			break;

		/* ĩ */
		case 'X': case 'x':
			words[i] = L'\x0129';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'O':
		switch (words[i+1]){
		/* Ò */
		case 'F': case 'f':
			words[i] = L'\x00D2';
			clear_one_element(words, i);
			break;

		/* Ọ */
		case 'J': case 'j':
			words[i] = L'\x1ECC';
			clear_one_element(words, i);
			break;
		
		/* Ô */
		case 'O': case 'o':
			words[i] = L'\x00D4';

			switch (words[i+2]){
			/* Ồ */
			case 'F': case 'f':
				words[i] = L'\x1ED2';
				clear_two_element(words, i);
				break;

			/* Ộ */
			case 'J': case 'j':
				words[i] = L'\x1ED8';
				clear_two_element(words, i);
				break;

			/* Ổ */
			case 'R': case 'r':
				words[i] = L'\x1ED4';
				clear_two_element(words, i);
				break;

			/* Ố */
			case 'S': case 's':
				words[i] = L'\x1ED0';
				clear_two_element(words, i);
				break;

			/* Ỗ */
			case 'X': case 'x':
				words[i] = L'\x1ED6';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;

		/* Ỏ */
		case 'R': case 'r':
			words[i] = L'\x1ECE';
			clear_one_element(words, i);
			break;

		/* Ó */
		case 'S': case 's':
			words[i] = L'\x00D3';
			clear_one_element(words, i);
			break;

		/* Ơ */
		case 'W': case 'w':
			words[i] = L'\x01A0';

			switch (words[i+2]){
			/* Ờ */
			case 'F': case 'f':
				words[i] = L'\x1EDC';
				clear_two_element(words, i);
				break;

			/* Ợ */
			case 'J': case 'j':
				words[i] = L'\x1EE2';
				clear_two_element(words, i);
				break;

			/* Ở */
			case 'R': case 'r':
				words[i] = L'\x1EDE';
				clear_two_element(words, i);
				break;

			/* Ớ */
			case 'S': case 's':
				words[i] = L'\x1EDA';
				clear_two_element(words, i);
				break;

			/* Ỡ */
			case 'X': case 'x':
				words[i] = L'\x1EE0';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;
				
		/* Õ */
		case 'X': case 'x':
			words[i] = L'\x00D5';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'o':
		switch (words[i+1]){
		/* ò */
		case 'F': case 'f':
			words[i] = L'\x00F2';
			clear_one_element(words, i);
			break;

		/* ọ */
		case 'J': case 'j':
			words[i] = L'\x1ECD';
			clear_one_element(words, i);
			break;
			
		/* ô */
		case 'O': case 'o':
			words[i] = L'\x00F4';

			switch (words[i+2]){
			/* ồ */
			case 'F': case 'f':
				words[i] = L'\x1ED3';
				clear_two_element(words, i);
				break;

			/* ộ */
			case 'J': case 'j':
				words[i] = L'\x1ED9';
				clear_two_element(words, i);
				break;

			/* ổ */
			case 'R': case 'r':
				words[i] = L'\x1ED5';
				clear_two_element(words, i);
				break;

			/* ố */
			case 'S': case 's':
				words[i] = L'\x1ED1';
				clear_two_element(words, i);
				break;

			/* ỗ */
			case 'X': case 'x':
				words[i] = L'\x1ED7';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;

		/* ỏ */
		case 'R': case 'r':
			words[i] = L'\x1ECF';
			clear_one_element(words, i);
			break;

		/* ó */
		case 'S': case 's':
			words[i] = L'\x00F3';
			clear_one_element(words, i);
			break;

		/* ơ */
		case 'W': case 'w':
			words[i] = L'\x01A1';

			switch (words[i+2]){
			/* ờ */
			case 'F': case 'f':
				words[i] = L'\x1EDD';
				clear_two_element(words, i);
				break;

			/* ợ */
			case 'J': case 'j':
				words[i] = L'\x1EE3';
				clear_two_element(words, i);
				break;

			/* ở */
			case 'R': case 'r':
				words[i] = L'\x1EDF';
				clear_two_element(words, i);
				break;

			/* ớ */
			case 'S': case 's':
				words[i] = L'\x1EDB';
				clear_two_element(words, i);
				break;

			/* ỡ */
			case 'X': case 'x':
				words[i] = L'\x1EE1';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;
				
		/* õ */
		case 'X': case 'x':
			words[i] = L'\x00F5';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'U':
		switch (words[i+1]){
		/* Ù */
		case 'F': case 'f':
			words[i] = L'\x00D9';
			clear_one_element(words, i);
			break;

		/* Ụ */
		case 'J': case 'j':
			words[i] = L'\x1EE4';
			clear_one_element(words, i);
			break;

		/* Ủ */
		case 'R': case 'r':
			words[i] = L'\x1EE6';
			clear_one_element(words, i);
			break;

		/* Ú */
		case 'S': case 's':
			words[i] = L'\x00DA';
			clear_one_element(words, i);
			break;

		/* Ư */
		case 'W': case 'w':
			words[i] = L'\x01AF';

			switch (words[i+2]){
			/* Ừ */
			case 'F': case 'f':
				words[i] = L'\x1EEA';
				clear_two_element(words, i);
				break;

			/* Ự */
			case 'J': case 'j':
				words[i] = L'\x1EF0';
				clear_two_element(words, i);
				break;
						
			/* Ử */
			case 'R': case 'r':
				words[i] = L'\x1EEC';
				clear_two_element(words, i);
				break;


			/* Ứ */
			case 'S': case 's':
				words[i] = L'\x1EE8';
				clear_two_element(words, i);
				break;

			/* Ữ */
			case 'X': case 'x':
				words[i] = L'\x1EEE';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;		
			}
			break;

		/* Ũ */
		case 'X': case 'x':
			words[i] = L'\x0168';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'u':
		switch (words[i+1]){
		/* ù */
		case 'F': case 'f':
			words[i] = L'\x00F9';
			clear_one_element(words, i);
			break;

		/* ụ */
		case 'J': case 'j':
			words[i] = L'\x1EE5';
			clear_one_element(words, i);
			break;

		/* ủ */
		case 'R': case 'r':
			words[i] = L'\x1EE7';
			clear_one_element(words, i);
			break;

		/* ú */
		case 'S': case 's':
			words[i] = L'\x00FA';
			clear_one_element(words, i);
			break;

		/* ư */
		case 'W': case 'w':
			words[i] = L'\x01B0';

			switch (words[i+2]){
			/* ừ */
			case 'F': case 'f':
				words[i] = L'\x1EEB';
				clear_two_element(words, i);
				break;

			/* ự */
			case 'J': case 'j':
				words[i] = L'\x1EF1';
				clear_two_element(words, i);
				break;
						
			/* ử */
			case 'R': case 'r':
				words[i] = L'\x1EED';
				clear_two_element(words, i);
				break;


			/* ứ */
			case 'S': case 's':
				words[i] = L'\x1EE9';
				clear_two_element(words, i);
				break;

			/* ữ */
			case 'X': case 'x':
				words[i] = L'\x1EEF';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;		
			}
			break;

		/* ũ */
		case 'X': case 'x':
			words[i] = L'\x0169';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'Y':
		switch(words[i+1]){
		/* Ỳ */
		case 'F': case 'f':
			words[i] = L'\x1EF2';
			clear_one_element(words, i);
			break;

		/* Ỵ */
		case 'J': case 'j':
			words[i] = L'\x1EF4';
			clear_one_element(words, i);
			break;

		/* Ỷ */
		case 'R': case 'r':
			words[i] = L'\x1EF6';
			clear_one_element(words, i);
			break;

		/* Ý */
		case 'S': case 's':
			words[i] = L'\x00DD';
			clear_one_element(words, i);
			break;

		/* Ỹ */
		case 'X': case 'x':
			words[i] = L'\x1EF8';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'y':
		switch (words[i+1]){
		/* ỳ */
		case 'F': case 'f':
			words[i] = L'\x1EF3';
			clear_one_element(words, i);
			break;

		/* ỵ */
		case 'J': case 'j':
			words[i] = L'\x1EF5';
			clear_one_element(words, i);
			break;

		/* ỷ */
		case 'R': case 'r':
			words[i] = L'\x1EF7';
			clear_one_element(words, i);
			break;

		/* ý */
		case 'S': case 's':
			words[i] = L'\x00FD';
			clear_one_element(words, i);
			break;

		/* ỹ */
		case 'X': case 'x':
			words[i] = L'\x1EF9';
			clear_one_element(words, i);
			break;
		}
		break;
	}
}

void
vni_method(wchar_t *words, unsigned int i)
{
	switch (words[i]){
	case 'A':
		switch (words[i+1]){
		/* Â */
		case '6':
			words[i] = L'\x00C2';
			
			switch (words[i+2]){
			/* Ầ */
			case '2':
				words[i] = L'\x1EA6';
				clear_two_element(words, i);
				break;

			/* Ậ */	
			case '5':
				words[i] = L'\x01EAC';
				clear_two_element(words, i);
				break;

			/* Ẩ */
			case '3':
				words[i] = L'\x1EA8';
				clear_two_element(words, i);
				break;

			/* Ấ */
			case '1':
				words[i] = L'\x1EA4';
				clear_two_element(words, i);
				break;

			/* Ẫ */
			case '4':
				words[i] = L'\x1EAA';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;
				
		/* À */
		case '2':
			words[i] = L'\x00C0';
			clear_one_element(words, i);
			break;

		/* Ạ */	
		case '5':
			words[i] = L'\x1EA0';
			clear_one_element(words, i);
			break;

		/* Ả */
		case '3':
			words[i] = L'\x1EA2';
			clear_one_element(words, i);
			break;

		/* Á */
		case '1':
			words[i] = L'\x00C1';
			clear_one_element(words, i);
			break;
					
		/* Ă */
		case '8':
			words[i] = L'\x0102';
			
			switch (words[i+2]){
			/* Ằ */
			case '2':
				words[i] = L'\x1EB0';
				clear_two_element(words, i);
				break;

			/* Ặ */
			case '5':
				words[i] = L'\x1EB6';
				clear_two_element(words, i);
				break;

			/* Ẳ */
			case '3':
				words[i] = L'\x1EB2';
				clear_two_element(words, i);
				break;

			/* Ắ */
			case '1':
				words[i] = L'\x1EAE';
				clear_two_element(words, i);
				break;

			/* Ẵ */
			case '4':
				words[i] = L'\x1EB4';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;

		/* Ã */
		case '4':
			words[i] = L'\x00C3';
			clear_one_element(words, i);
	       		break;	       
		}
		break;

	case 'a':
		switch (words[i+1]){
		/* â */
		case '6':
			words[i] = L'\x00E2';
					
			switch( words[i+2]){
			/* ầ */
			case '2':
				words[i] = L'\x1EA7';
				clear_two_element(words, i);
				break;
						
			/* ậ */
			case '5':
				words[i] = L'\x1EAD';
				clear_two_element(words, i);
				break;

			/* ẩ */
			case '3':
				words[i] = L'\x1EA9';
				clear_two_element(words, i);
				break;

			/* ấ */
			case '1':
				words[i] = L'\x1EA5';
				clear_two_element(words, i);
				break;

			/* ẫ */
			case '4':
				words[i] = L'\x1EAB';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;

		/* à */
		case '2':
			words[i] = L'\x00E0';
			clear_one_element(words, i);
			break;
			
		/* ạ */
		case '5':
			words[i] = L'\x1EA1';
			clear_one_element(words, i);
			break;

		/* ả */
		case '3':
			words[i] = L'\x1EA3';
			clear_one_element(words, i);
			break;

		/* á */
		case '1':
			words[i] = L'\x00E1';
			clear_one_element(words, i);
			break;
			
		/* ă */	
		case '8':
			words[i] = L'\x0103';
			
			switch (words[i+2]){
			/* ằ */
			case '2':
				words[i] = L'\x1EB1';
				clear_two_element(words, i);
				break;

			/* ặ */
			case '5':
				words[i] = L'\x1EB7';
				clear_two_element(words, i);
				break;

			/* ẳ */
			case '3':
				words[i] = L'\x1EB3';
				clear_two_element(words, i);
				break;

			/* ắ */
			case '1':
				words[i] = L'\x1EAF';
				clear_two_element(words, i);
				break;

			/* ẵ */
			case '4':
				words[i] = L'\x1EB5';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;

		/* ã */
		case '4':
			words[i] = L'\x00E3';
			clear_one_element(words, i);
	       		break;	       
		}
		break;

	/* Đ */
	case 'D':
		if (words[i+1] == '9'){
			words[i] = L'\x0110';
			clear_one_element(words, i);
		}
		break;

	/* đ */
	case 'd':
		if (words[i+1] == '9'){
			words[i] = L'\x0111';
			clear_one_element(words, i);
		}
		break;


	case 'E':
		switch (words[i+1]){
		/* Ê */
		case '6':
			words[i] = L'\x00CA';

			switch (words[i+2]) {
			/* Ề */ 
			case '2':
				words[i] = L'\x1EC0';
				clear_two_element(words, i);
				break;
					
			/* Ệ */	
			case '5':
				words[i] = L'\x1EC6';
				clear_two_element(words, i);
				break;
						
			/* Ể */
			case '3':
				words[i] = L'\x1EC2';
				clear_two_element(words, i);
				break;

			/* Ế */
			case '1':
				words[i] = L'\x1EBE';
				clear_two_element(words, i);
				break;

			/* Ễ */
			case '4':
				words[i] = L'\x1EC4';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;	
			}
			break;

		/* È */
		case '2':
			words[i] = L'\x00C8';
			clear_one_element(words, i);
			break;

		/* Ẹ */
		case '5':
			words[i] = L'\x1EB8';
			clear_one_element(words, i);
			break;

		/* Ẻ */
		case '3':
			words[i] = L'\x1EBA';
			clear_one_element(words, i);
			break;

		/* É */
		case '1':
			words[i] = L'\x00C9';
			clear_one_element(words, i);
			break;

		/* Ẽ */
		case '4':
			words[i] = L'\x1EBC';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'e':
		switch (words[i+1]){
		/* ê */
		case '6':
			words[i] = L'\x00EA';

			switch (words[i+2]){
			/* ề */
			case '2':
				words[i] = L'\x1EC1';
				clear_two_element(words, i);
				break;

			/* ệ */
			case '5':
				words[i] = L'\x1EC7';
				clear_two_element(words, i);
				break;

			/* ể */
			case '3':
				words[i] = L'\x1EC3';
				clear_two_element(words, i);
				break;
						
			/* ế */
			case '1':
				words[i] = L'\x1EBF';
				clear_two_element(words, i);
				break;

			/* ễ */
			case '4':
				words[i] = L'\x1EC5';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;
			
		/* è */
		case '2':
			words[i] = L'\x00E8';
			clear_one_element(words, i);
			break;

		/* ẹ */
		case '5':
			words[i] = L'\x1EB9';
			clear_one_element(words, i);
			break;

		/* ẻ */
		case '3':
			words[i] = L'\x1EBB';
			clear_one_element(words, i);
			break;

		/* é */
		case '1':
			words[i] = L'\x00E9';
			clear_one_element(words, i);
			break;

		/* ẽ */
		case '4':
			words[i] = L'\x1EBD';	
			clear_one_element(words, i);
			break;
		}
		break;

	case 'I':
		switch (words[i+1]){
		/* Ì */
		case '2':
			words[i] = L'\x00CC';
			clear_one_element(words, i);
			break;

		/* Ị */
		case '5':
			words[i] = L'\x1ECA';
			clear_one_element(words, i);
			break; 
		
		/* Ỉ */
		case '3':
			words[i] = L'\x1EC8';
			clear_one_element(words, i);
			break;

		/* Í */
		case '1':
			words[i] = L'\x00CD';
			clear_one_element(words, i);
			break;

		/* Ĩ */
		case '4':
			words[i] = L'\x0128';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'i':
		switch (words[i+1]){
		/* ì */
		case '2':
			words[i] = L'\x00EC';
			clear_one_element(words, i);
			break;	

		/* ị */
		case '5':
			words[i] = L'\x1ECB';
			clear_one_element(words, i);
			break;

		/* ỉ */
		case '3':
			words[i] = L'\x1EC9';
			clear_one_element(words, i);
			break;

		/* í */
		case '1':
			words[i] = L'\x00ED';
			clear_one_element(words, i);
			break;

		/* ĩ */
		case '4':
			words[i] = L'\x0129';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'O':
		switch (words[i+1]){
		/* Ò */
		case '2':
			words[i] = L'\x00D2';
			clear_one_element(words, i);
			break;

		/* Ọ */
		case '5':
			words[i] = L'\x1ECC';
			clear_one_element(words, i);
			break;
			
		/* Ô */
		case '6':
			words[i] = L'\x00D4';

			switch (words[i+2]){
			/* Ồ */
			case '2':
				words[i] = L'\x1ED2';
				clear_two_element(words, i);
				break;

			/* Ộ */
			case '5':
				words[i] = L'\x1ED8';
				clear_two_element(words, i);
				break;

			/* Ổ */
			case '3':
				words[i] = L'\x1ED4';
				clear_two_element(words, i);
				break;

			/* Ố */
			case '1':
				words[i] = L'\x1ED0';
				clear_two_element(words, i);
				break;

			/* Ỗ */
			case '4':
				words[i] = L'\x1ED6';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;

		/* Ỏ */
		case '3':
			words[i] = L'\x1ECE';
			clear_one_element(words, i);
			break;

		/* Ó */
		case '1':
			words[i] = L'\x00D3';
			clear_one_element(words, i);
			break;

		/* Ơ */
		case '7':
			words[i] = L'\x01A0';

			switch (words[i+2]){
			/* Ờ */
			case '2':
				words[i] = L'\x1EDC';
				clear_two_element(words, i);
				break;

			/* Ợ */
			case '5':
				words[i] = L'\x1EE2';
				clear_two_element(words, i);
				break;

			/* Ở */
			case '3':
				words[i] = L'\x1EDE';
				clear_two_element(words, i);
				break;

			/* Ớ */
			case '1':
				words[i] = L'\x1EDA';
				clear_two_element(words, i);
				break;

			/* Ỡ */
			case '4':
				words[i] = L'\x1EE0';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;
				
		/* Õ */
		case '4':
			words[i] = L'\x00D5';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'o':
		switch (words[i+1]){
		/* ò */
		case '2':
			words[i] = L'\x00F2';
			clear_one_element(words, i);
			break;

		/* ọ */
		case '5':
			words[i] = L'\x1ECD';
			clear_one_element(words, i);
			break;
			
		/* ô */
		case '6':
			words[i] = L'\x00F4';

			switch (words[i+2]){
			/* ồ */
			case '2':
				words[i] = L'\x1ED3';
				clear_two_element(words, i);
				break;

			/* ộ */
			case '5':
				words[i] = L'\x1ED9';
				clear_two_element(words, i);
				break;

			/* ổ */
			case '3':
				words[i] = L'\x1ED5';
				clear_two_element(words, i);
				break;

			/* ố */
			case '1':
				words[i] = L'\x1ED1';
				clear_two_element(words, i);
				break;

			/* ỗ */
			case '4':
				words[i] = L'\x1ED7';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;

		/* ỏ */
		case '3':
			words[i] = L'\x1ECF';
			clear_one_element(words, i);
			break;

		/* ó */
		case '1':
			words[i] = L'\x00F3';
			clear_one_element(words, i);
			break;

		/* ơ */
		case '7':
			words[i] = L'\x01A1';

			switch (words[i+2]){
			/* ờ */
			case '2':
				words[i] = L'\x1EDD';
				clear_two_element(words, i);
				break;

			/* ợ */
			case '5':
				words[i] = L'\x1EE3';
				clear_two_element(words, i);
				break;

			/* ở */
			case '3':
				words[i] = L'\x1EDF';
				clear_two_element(words, i);
				break;

			/* ớ */
			case '1':
				words[i] = L'\x1EDB';
				clear_two_element(words, i);
				break;

			/* ỡ */
			case '4':
				words[i] = L'\x1EE1';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;
			}
			break;
			
		/* õ */
		case '4':
			words[i] = L'\x00F5';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'U':
		switch (words[i+1]){
		/* Ù */
		case '2':
			words[i] = L'\x00D9';
			clear_one_element(words, i);
			break;

		/* Ụ */
		case '5':
			words[i] = L'\x1EE4';
			clear_one_element(words, i);
			break;

		/* Ủ */
		case '3':
			words[i] = L'\x1EE6';
			clear_one_element(words, i);
			break;

		/* Ú */
		case '1':
			words[i] = L'\x00DA';
			clear_one_element(words, i);
			break;

		/* Ư */
		case '7':
			words[i] = L'\x01AF';

			switch (words[i+2]){
			/* Ừ */
			case '2':
				words[i] = L'\x1EEA';
				clear_two_element(words, i);
				break;

			/* Ự */
			case '5':
				words[i] = L'\x1EF0';
				clear_two_element(words, i);
				break;
						
			/* Ử */
			case '3':
				words[i] = L'\x1EEC';
				clear_two_element(words, i);
				break;


			/* Ứ */
			case '1':
				words[i] = L'\x1EE8';
				clear_two_element(words, i);
				break;

			/* Ữ */
			case '4':
				words[i] = L'\x1EEE';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;		
			}
			break;

		/* Ũ */
		case '4':
			words[i] = L'\x0168';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'u':
		switch (words[i+1]){
		/* ù */
		case '2':
			words[i] = L'\x00F9';
			clear_one_element(words, i);
			break;

		/* ụ */
		case '5':
			words[i] = L'\x1EE5';
			clear_one_element(words, i);
			break;

		/* ủ */
		case '3':
			words[i] = L'\x1EE7';
			clear_one_element(words, i);
			break;

		/* ú */
		case '1':
			words[i] = L'\x00FA';
			clear_one_element(words, i);
			break;

		/* ư */
		case '7':
			words[i] = L'\x01B0';

			switch (words[i+2]){
			/* ừ */
			case '2':
				words[i] = L'\x1EEB';
				clear_two_element(words, i);
				break;

			/* ự */
			case '5':
				words[i] = L'\x1EF1';
				clear_two_element(words, i);
				break;
						
			/* ử */
			case '3':
				words[i] = L'\x1EED';
				clear_two_element(words, i);
				break;


			/* ứ */
			case '1':
				words[i] = L'\x1EE9';
				clear_two_element(words, i);
				break;

			/* ữ */
			case '4':
				words[i] = L'\x1EEF';
				clear_two_element(words, i);
				break;

			default:
				clear_one_element(words, i);
				break;		
			}
			break;

		/* ũ */
		case '4':
			words[i] = L'\x0169';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'Y':
		switch (words[i+1]){
		/* Ỳ */
		case '2':
			words[i] = L'\x1EF2';
			clear_one_element(words, i);
			break;

		/* Ỵ */
		case '5':
			words[i] = L'\x1EF4';
			clear_one_element(words, i);
			break;

		/* Ỷ */
		case '3':
			words[i] = L'\x1EF6';
			clear_one_element(words, i);
			break;

		/* Ý */
		case '1':
			words[i] = L'\x00DD';
			clear_one_element(words, i);
			break;

		/* Ỹ */
		case '4':
			words[i] = L'\x1EF8';
			clear_one_element(words, i);
			break;
		}
		break;

	case 'y':
		switch (words[i+1]){
		/* ỳ */
		case '2':
			words[i] = L'\x1EF3';
			clear_one_element(words, i);
			break;

		/* ỵ */
		case '5':
			words[i] = L'\x1EF5';
			clear_one_element(words, i);
			break;

		/* ỷ */
			case '3':
			words[i] = L'\x1EF7';
			clear_one_element(words, i);
			break;

		/* ý */
		case '1':
			words[i] = L'\x00FD';
			clear_one_element(words, i);
			break;

		/* ỹ */
		case '4':
			words[i] = L'\x1EF9';
			clear_one_element(words, i);
			break;
		}
		break;
	}
}
